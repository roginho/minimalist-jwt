<?php

use Minimalist\Jwt\AlgorithmEnum;
use Minimalist\Jwt\Jwt;
use PHPUnit\Framework\TestCase;

class JwtTest extends TestCase
{

    /**
    * @dataProvider dadosValidos
    */
    public function testJwtValido($dados)
    {
        $jwt = new Jwt($dados['type']);
        $token = $jwt->generate($dados['payload'], $dados['secret']);
        $valido = $jwt->isValid($token, $dados['secret']);
        $this->assertTrue($valido);
    }


    /**
    * @dataProvider dadosValidos
    */
    public function testJwtInvalido($dados)
    {
        $jwt = new Jwt(AlgorithmEnum::SHA_256);
        $token = $jwt->generate($dados['payload'], $dados['secret']);
        $valido = $jwt->isValid($token, 'secretA');
        $this->assertFalse($valido);
        $valido2 = $jwt->isValid($token.'x', 'secret');
        $this->assertFalse($valido2);
    }


    public function dadosValidos()
    {
        $payload = array('sub'=>'1234567890','name'=>'Róger Soares', 'admin'=>true, 'exp'=>(time() + 60));
        $secret = "secret";
        return [
            'HMAC' => [['type' => AlgorithmEnum::HMAC, 'payload'=>$payload, 'secret'=>$secret]],
            'SHA_256' => [['type' => AlgorithmEnum::SHA_256, 'payload'=>$payload, 'secret'=>$secret]],
            'RSA' => [['type' => AlgorithmEnum::RSA,'payload'=>$payload, 'secret'=>$secret]]

        ];
    }
}
