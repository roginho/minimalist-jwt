<?php

namespace Minimalist\Jwt;

enum AlgorithmEnum: string
{
    case HMAC = 'HMAC';
    case SHA_256 = 'SHA-256';
    case RSA = 'RSA';
}