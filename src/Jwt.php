<?php

namespace Minimalist\Jwt;

class Jwt
{

    private const HASH_HMAC = 'SHA256';
    private const ALG = 'HS256';
    private const TYP = 'JWT';
    private $headers = [];

    public function __construct(AlgorithmEnum $algorithmEnum) {
        $this->headers = json_encode([
            "alg" => $algorithmEnum->value,
            "typ" => self::TYP
        ]);
    }
    public function generate(array $payload, string $secret): string {
        
        $headersEncoded = $this->base64urlEncode(json_encode($this->headers));
        $payloadEncoded = $this->base64urlEncode(json_encode($payload));
        $signature = hash_hmac(self::HASH_HMAC, $headersEncoded. '.' .$payloadEncoded, $secret, true);
        $signatureEncoded = $this->base64urlEncode($signature);
        $jwt = $headersEncoded. '.' .$payloadEncoded. '.' .$signatureEncoded;
        return $jwt;
    }

    public function isValid(string $token, string $secret) {
        $tokenParts = explode('.', $token);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signatureProvided = $tokenParts[2];

        $expiration = json_decode($payload)->exp;
        $isTokenExpired = ($expiration - time()) < 0;

        $base64UrlHeader = $this->base64urlEncode($header);
        $base64UrlPayload = $this->base64urlEncode($payload);
        $signature = hash_hmac(self::HASH_HMAC, $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        $base64UrlSignature = $this->base64urlEncode($signature);

        $isSignatureValid = ($base64UrlSignature === $signatureProvided);

        if ($isTokenExpired || !$isSignatureValid) {
            return false;
        }
        return true;

    }


    private function base64urlEncode(string $str): string {
        return rtrim(strtr(base64_encode($str), '+/', '-_'), '=');
    }
}
